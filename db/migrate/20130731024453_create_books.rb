class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :title
      t.date :released_date
      t.integer :author_id
      t.integer :rating
      t.boolean :available

      t.timestamps
    end
  end
end
